#Need ID Column (int) and will have primary key on it
#Need Title (str).
#Need Image (str).
    #Short description for preview (str).
#Project description (str).
#Project Date (date).

from sqlalchemy import Column, String, Integer, Date
from common.DAO import DAO

Base = DAO.get_base_mapper()

class Project(Base):
    """
    The preview and the page for the projects to go on the website.
    """

    __tablename__ = 'project'

    id: int = Column(
        Integer(),
        nullable=False,
        primary_key=True
    )

    title: str = Column(
        String(100),
        nullable=False
    )

    image: str = Column(
        String(200),
        nullable=False
    )

    short_description: str = Column(
        String(200),
        nullable=False
    )

    full_description: str = Column(
        String(20000),
        nullable=False
    )

    project_date: str = Column(
        Date(),
        nullable=False
    )

    source_link: str = Column(
        String(500),
        nullable=True
    )

    def to_dict (self):
        return {
            "id": self.id,
            "title": self.title,
            "image": self.image,
            "short_description": self.short_description,
            "full_description": self.full_description,
            "project_date": self.project_date,
            "source_link": self.source_link
        }

