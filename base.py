from flask import Flask, request, render_template, send_from_directory
from models.project import Project
from common.DAO import DAO
import os
app = Flask(__name__)

@app.route("/")
def home():

    return render_template("home.html")

@app.route("/projects")
def projects():

    with DAO.session_scope(os.getenv("DATABASEURL")) as session:
        projects = session.query(Project).all()
        new_projects = []
        for project in projects:
            new_projects.append(project.to_dict())

    return render_template("projects.html", projects = new_projects)

@app.route("/projects/<id>")
def project(id):

    with DAO.session_scope(os.getenv("DATABASEURL")) as session:
        project = session.query(Project).get(id)
        new_project = project.to_dict()
        

    return render_template("project.html", project = new_project)

@app.route("/books")
def books():

    return render_template("books.html")

@app.route('/media/<path:filename>')
def download_file(filename):
    return send_from_directory("media", filename)

if __name__ == '__main__':
    app.run()
    

